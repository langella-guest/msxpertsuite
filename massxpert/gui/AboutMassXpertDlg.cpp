/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QFile>


/////////////////////// Local includes
#include <massxpert/gui/AboutMassXpertDlg.hpp>


namespace msXpSmassXpert
{

AboutMassXpertDlg::AboutMassXpertDlg(QWidget *parent,
                                     const QString &applicationName)
  : AboutDlg{parent, applicationName}
{
  // Set the feature text that is specific of this application.
  setApplicationFeaturesText();
  setHistoryText();
}


AboutMassXpertDlg::~AboutMassXpertDlg()
{
}


void
AboutMassXpertDlg::setApplicationFeaturesText()
{
  m_ui.featuresTextEdit->setHtml(
    "<html><head><meta name=\"qrichtext\" content=\"1\" /><style "
    "type=\"text/css\">\n"
    " p, li { white-space: pre-wrap; }\n"
    " </style></head><body style=\" font-family:'Sans Serif'; font-size:12pt; "
    "font-weight:400; font-style:normal; text-decoration:none;\">\n"
    "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; "
    "margin-left:0px; margin-right:0px; -qt-block-indent:0; "
    "text-indent:0px;\"></p>\n"
    "<br>\n"
    "<br>\n"
    "<em>massXpert</em> provides the following main features:\n"
    "<br>\n"
    "<ul>\n"
    "<li>Highly configurable sequence editor using user-defined monomer "
    "vignettes;</li>\n"
    "<li>Definition of atoms with any number of isotopic mass/abundance "
    "pairs;</li>\n"
    "<li>Definition of brand new polymer chemistries;</li>\n"
    "<li>Polymer sequence cleavage using user-defined flexible cleavage "
    "agents;</li>\n"
    "<li>Gas-phase fragmentation of oligomers using user-defined flexible "
    "fragmentation rules;</li>\n"
    "<li>Powerful mass search in a polymer sequence with associated ion charge "
    "values ranges;</li>\n"
    "<li>Powerful Copy/Paste paradigm allowing the pasted polymer sequence to "
    "be purified;</li>\n"
    "<li>Calculation of isotopic pattern of any chemical starting from a "
    "formula.</li>\n"
    "<li>Spectrum generation on the basis of polymer digestion products.</li>\n"
    "</ul>\n"
    "<br>\n"
    "Visit http://www.msxpertsuite.org or "
    "https://salsa.debian.org/lopippo/msXpertSuite.\n"
    "</body></html>\n");
}


void
AboutMassXpertDlg::setHistoryText()
{

  // We load the history file now. It is part of the source code in the form
  // of libmass/history.html.

  QFile historyFile(":/doc/history.html");

  if(!historyFile.open(QFile::ReadOnly | QFile::Text))
    {
      qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);
    }

  QString history = historyFile.readAll();
  m_ui.historyTextEdit->setHtml(history);
}


} // namespace msXpSmassXpert
