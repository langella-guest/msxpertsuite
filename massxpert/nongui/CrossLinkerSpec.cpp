/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QFile>
#include <QDebug>


/////////////////////// Local includes
#include <massxpert/nongui/CrossLinkerSpec.hpp>


namespace msXpSmassXpert
{

void
CrossLinkerSpec::setName(const QString &name)
{
  m_name = name;
}


const QString &
CrossLinkerSpec::name()
{
  return m_name;
}

void
CrossLinkerSpec::setVector(const QString &vector)
{
  m_vector = vector;
}


const QString &
CrossLinkerSpec::vector()
{
  return m_vector;
}


void
CrossLinkerSpec::setSound(const QString &sound)
{
  m_sound = sound;
}


const QString &
CrossLinkerSpec::sound()
{
  return m_sound;
}


bool
CrossLinkerSpec::parseFile(QString &filePath,
                           QList<CrossLinkerSpec *> *specList)
{
  CrossLinkerSpec *crossLinkerSpec = 0;

  qint64 lineLength;

  QString line;
  QString temp;

  char buffer[1024];

  int percentSignIdx = 0;

  Q_ASSERT(specList != 0);

  if(filePath.isEmpty())
    return false;

  QFile file(filePath);

  if(!file.open(QFile::ReadOnly))
    return false;

  // The lines we have to parse are of the following type:
  // DisulfideBon%disulfidebond.svg
  // Any line starting with ' ' or '#' are not parsed.

  // Get the first line of the file. Next we enter in to a
  // while loop.

  lineLength = file.readLine(buffer, sizeof(buffer));

  while(lineLength != -1)
    {
      // The line is now in buffer, and we want to convert
      // it to Unicode by setting it in a QString.
      line = buffer;

      // The line that is in line should contain something like:
      // DisulfideBon%disulfidebond.svg
      //
      // Note, however that lines beginning with either '\n'(newline)
      // or '#' are comments.

      // Remove all the spaces from the borders: Whitespace means any
      // character for which QChar::isSpace() returns true. This
      // includes the ASCII characters '\t', '\n', '\v', '\f', '\r',
      // and ' '.

      line = line.trimmed();

      if(line.isEmpty() || line.startsWith('#', Qt::CaseInsensitive))
        {
          lineLength = file.readLine(buffer, sizeof(buffer));
          continue;
        }

      // Now some other checks. Remember the format of the line:
      // DisulfideBon%disulfidebond.svg

      percentSignIdx = line.indexOf('%', 0, Qt::CaseInsensitive);

      if(percentSignIdx == -1 || line.count('%', Qt::CaseInsensitive) > 2)
        return false;

      // OK, we finally can allocate a new CrossLinkerSpec *.
      crossLinkerSpec = new CrossLinkerSpec();

      crossLinkerSpec->m_name = line.left(percentSignIdx);

      // Remove from the line the "DisulfideBond%" substring, as we
      // do not need it anymore.
      line.remove(0, percentSignIdx + 1);

      // Now we can go on with the graphics files stuff. At this point
      // all that remains up to the end of the line is the filename
      // with extension .svg.

      if(!line.endsWith(".svg", Qt::CaseSensitive))
        {
          delete crossLinkerSpec;
          return false;
        }

      // Ok that's done, we can get the vector graphics filename:

      crossLinkerSpec->m_vector = line;

      specList->append(crossLinkerSpec);

      // Continue to next line to further parse the file.
      lineLength = file.readLine(buffer, sizeof(buffer));
    }

  return true;
}

} // namespace msXpSmassXpert
