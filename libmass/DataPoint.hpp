/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QString>
#include <QMetaType>

/////////////////////// Local includes


namespace msXpSlibmass
{

class DataPointJs;

//! The DataPoint class provides a (x,y) point paradigm.
/*!

  An (x,y) data point may describe any data element to be represented as a
  point in graph plot. That data point might be a (retention time, TIC
  intensity) pair or a (m/z value, intensity) pair or a (drift time,
  intensity) pair, for example.

  The member variables, m_key and m_val, are initialized with qSNaN() which
  then allows for easy validation (checking that the DataPoint was actually
  initialized with meaningful values).

  We use the (key,value) generic wording to represent the x,y values of a
  given point because the DataPoint class is most used withing the Trace
  class, that is used to represent any (key,value) pair-based trace (mass
  spectrum, drift spectrum, TIC chromatogram...).

  This class is massively used by the Trace class.
  As such Trace is declared friend of DataPoint.

*/
class DataPoint
{
  friend class Trace;
  friend class MassSpectrum;
  friend class DataPointJs;

  private:
  //! Key of the data point, this is the x-axis value when thinking Trace
  //! (Cartesian) plots.

  double m_key = qSNaN();

  //! Value of the data point, this is the y-axis value when thinking Trace
  //! (Cartesian) plots.

  double m_val = qSNaN();

  double &rval();

  public:
  DataPoint();
  DataPoint(const DataPoint &other);
  DataPoint(double key, double val);
  DataPoint(const QString &xyLine);
  virtual ~DataPoint();

  DataPoint &operator=(const DataPoint &other);

  void initialize(const DataPoint &dataPoint);
  bool initialize(const QString &xyLine);
  void initialize(double key, double value);

  void setKey(double key);
  double key() const;
  void setVal(double value);
  double val() const;

  virtual bool isValid() const;
};

} // namespace msXpSlibmass


Q_DECLARE_METATYPE(msXpSlibmass::DataPoint);
extern int dataPointMetaTypeId;
