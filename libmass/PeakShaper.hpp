/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


//////////////////////////// Local includes
#include "PeakShaperConfig.hpp"
#include "DataPoint.hpp"
#include "Trace.hpp"


namespace msXpSlibmass
{

// The PeakShaper class contains all required data to compute a gaussian or
// lorentzian shape corresponding to a given centroid (x,y) with z=m/z and
// y=relative intensity. The data points that make the shape are stored as a
// Trace.

class PeakShaper
{
  private:
  DataPoint m_peakCentroid;
  PeakShaperConfig m_config;
  double m_relativeIntensity = std::numeric_limits<double>::infinity();
  Trace m_trace;

  void clearTrace();

  public:
  PeakShaper();
  PeakShaper(double mz, double i, const PeakShaperConfig &config);
  PeakShaper(const DataPoint &peakCentroid, const PeakShaperConfig &config);
  PeakShaper(const PeakShaper &);
  virtual ~PeakShaper();

  void setPeakCentroid(const DataPoint &data_point);
  const DataPoint &getPeakCentroid() const;

  void setConfig(const PeakShaperConfig &config);
  const PeakShaperConfig &getConfig() const;

  const Trace &getTrace() const;

  bool computeRelativeIntensity(double sum_probabilities);
  void setRelativeIntensity(double rel_int);
  double getRelativeIntensity() const;

  int computePeakShape();
  int computeGaussianPeakShape();
  int computeLorentzianPeakShape();

  double intensityAt(double mz, double tolerance, bool &ok);

  QString traceAsText();
  bool traceToFile(const QString &file_name);
};

} // namespace msXpSlibmass
