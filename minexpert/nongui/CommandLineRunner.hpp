/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// Qt includes
#include <QObject>
#include <QList>
#include <QStringList>


/////////////////////// Local includes
#include <minexpert/nongui/History.hpp>


namespace msXpSmineXpert
{

//! The CommandLineRunner class provides an interface to the command line
/*!

  When running the mineXpert program from the command line, the command line
  is analyzed and converted to actions by this CommandLineRunner class.

 */
class CommandLineRunner : public QObject
{
  Q_OBJECT

  public:
  //! Tells if the run is for converting file formats
  bool m_isFileConversion = false;

  //! Tells if the run is for exporting spectra to xy-formatted files
  bool m_isExportToXyFiles = false;

  //! Index of the first spectrum to export to xy-formatted file.
  int m_startIndex = -1;

  //! Index of the last spectrum to export to xy-formatted file.
  int m_endIndex = -1;

  //! Tells is the operations should be performed in streamed mode
  bool m_isStreamed = false;

  //! List of file names to be used as input
  QStringList m_inFileNames;

  //! File name of the output file
  QString m_outFileName;

  //! File name of the script (JavaScript) to be run
  QString m_scriptFileName;

  //! Tells if the output of the program is to be sent to the shell stdout
  bool m_isStdOut = false;

  //! When iterating in the input file name list, store the name in this
  //! variable
  QString m_currentInputFileName;

  //! Base message that is modified during the run for feedback to the user
  QString m_baseMessage;

  CommandLineRunner();
  virtual ~CommandLineRunner();

  QStringList existingInputFiles();
  bool allInputFilesExist(int *existingFiles, int *missingFiles);
  bool allInputFilesExist(QStringList *existingFiles,
                          QStringList *missingFiles);

  bool validateCommandLine(QStringList *existingFiles,
                           QStringList *missingFiles,
                           QString *errors,
                           bool *shouldRunGui);

  bool run(QString *errors);

  bool runConversion(QString *errors);
  bool runExportToXyFiles(QString *errors);

  QString asText();
};


} // namespace msXpSmineXpert
