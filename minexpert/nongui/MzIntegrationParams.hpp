/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// Qt includes


/////////////////////// Local includes
#include <globals/globals.hpp>
#include <minexpert/nongui/SavGolFilter.hpp>

namespace msXpSmineXpert
{


//! The MzIntegrationParams class provides the parameters definining how m/z
//! integartions must be performed.
/*!

  Depending on the various mass spectrometer vendors, the mass spectrometry
  data files are structured in different ways and the software for mass data
  format conversion from raw files to mzML or mzXML produce mass data
  characterized by different behaviours.

  The different characteristics of mass spectrometry data set are:

 * The size of the various mass spectra in the file is constant or variable;
 * The first m/z value of the various spectra is identical or not (that is,
 the spectra are root in a constant or variable root m/z value);
 * The m/z delta between two consecutive m/z values of a given spectrum are
 constant or variable;
 * The spectra contain or not 0-value m/z data points;

*/
class MzIntegrationParams
{

  public:
  msXpS::BinningType m_binningType = msXpS::BinningType::BINNING_TYPE_NONE;

  double m_binSize = qSNaN();
  msXpS::MassToleranceType m_binSizeType =
    msXpS::MassToleranceType::MASS_TOLERANCE_NONE;
  bool m_applyMzShift            = false;
  bool m_removeZeroValDataPoints = false;

  bool m_applySavGolFilter = false;
  SavGolParams m_savGolParams;

  // Construction / Destruction

  MzIntegrationParams();
  MzIntegrationParams(msXpS::BinningType binningType,
                      double binSize,
                      msXpS::MassToleranceType binSizeType,
                      bool applyMzShift,
                      bool removeZeroValDataPoints);

  MzIntegrationParams(const MzIntegrationParams &other);
  MzIntegrationParams(const SavGolParams &savGolParams);

  virtual ~MzIntegrationParams();

  void initializeSavGolParams(int nL              = 15,
                              int nR              = 15,
                              int m               = 4,
                              int lD              = 0,
                              bool convolveWithNr = false);
  void initializeSavGolParams(const SavGolParams &params);

  MzIntegrationParams &operator=(const MzIntegrationParams &other);
  MzIntegrationParams &initialize(const MzIntegrationParams &other);

  void reset();

  bool isValid();

  QString asText() const;
};


} // namespace msXpSmineXpert

Q_DECLARE_METATYPE(msXpSmineXpert::MzIntegrationParams);
Q_DECLARE_METATYPE(msXpSmineXpert::MzIntegrationParams *);

extern int mzIntegrationParamsMetaTypeId;
