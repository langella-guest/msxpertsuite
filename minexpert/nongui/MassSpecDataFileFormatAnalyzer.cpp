/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// Local includes
#include <minexpert/nongui/MassSpecDataFileFormatAnalyzer.hpp>
#include <minexpert/nongui/globals.hpp>
#include <minexpert/nongui/MassSpecDataFileLoaderXy.hpp>
#include <minexpert/nongui/MassSpecDataFileLoaderDx.hpp>
#include <minexpert/nongui/MassSpecDataFileLoaderMs1.hpp>
#include <minexpert/nongui/MassSpecDataFileLoaderPwiz.hpp>
#include <minexpert/nongui/MassSpecDataFileLoaderSqlite3.hpp>
#include <minexpert/nongui/MassSpecDataFileLoaderBrukerXy.hpp>


namespace msXpSmineXpert
{


//! Construct a MassSpecDataFileFormatAnalyzer instance
/*!

  \param fileName the name of the file of which the contents is to be analyzed.

*/
MassSpecDataFileFormatAnalyzer::MassSpecDataFileFormatAnalyzer(
  const QString &fileName)
  : m_fileName{fileName}
{
}


//! Destruct \c this MassSpecDataFileFormatAnalyzer instance.
MassSpecDataFileFormatAnalyzer::~MassSpecDataFileFormatAnalyzer()
{
}


//! Tells if the file format can be loaded.
/*!

  Analyzes the contents of the file \c m_fileName and tells if these data can
  be loaded.

  \return an integer containing an MassSpecDataFileFormat enumeration value
  corresponding to the file format.

  \sa MassSpecDataFileFormat (enumeration)

  \sa canLoadData()

*/
int
MassSpecDataFileFormatAnalyzer::canLoadData()
{
  return MassSpecDataFileFormatAnalyzer::canLoadData(m_fileName);
}


//! Tells if the file format can be loaded.
/*!

  Analyzes the contents of the file \p fileName and tells if these data can
  be loaded.

  \param fileName name of the file of which the contents must be analyzed.

  \return an integer containing an MassSpecDataFileFormat enumeration value
  corresponding to the file format or -1 if the file is either not found or is
  not a file.

  \sa MassSpecDataFileFormat

  \sa canLoadData()

*/
int
MassSpecDataFileFormatAnalyzer::canLoadData(const QString &fileName)
{
  QFileInfo fileInfo(fileName);

  if(!fileInfo.exists())
    return -1;

  if(!fileInfo.isFile())
    return -1;

  for(int iter = 0;
      iter < MassSpecDataFileFormat::MASS_SPEC_DATA_FILE_FORMAT_LAST;
      ++iter)
    {
      // qDebug() << __FILE__ << __LINE__ << "current file format:" << iter;

      if(iter == MassSpecDataFileFormat::MASS_SPEC_DATA_FILE_FORMAT_SQLITE3)
        {
          if(!MassSpecDataFileLoaderSqlite3::canLoadData(fileName))
            continue;
          else
            return iter;
        }
      else if(iter == MassSpecDataFileFormat::MASS_SPEC_DATA_FILE_FORMAT_MZML)
        {
          int res = MassSpecDataFileLoaderPwiz::canLoadData(fileName);
          if(!res)
            continue;
          else
            return res;
        }
      else if(iter == MassSpecDataFileFormat::MASS_SPEC_DATA_FILE_FORMAT_MZXML)
        {
          int res = MassSpecDataFileLoaderPwiz::canLoadData(fileName);
          if(!res)
            continue;
          else
            return res;
        }
      else if(iter == MassSpecDataFileFormat::MASS_SPEC_DATA_FILE_FORMAT_DX)
        {
          if(!MassSpecDataFileLoaderDx::canLoadData(fileName))
            continue;
          else
            return iter;
        }
      else if(iter ==
              MassSpecDataFileFormat::MASS_SPEC_DATA_FILE_FORMAT_BRUKER_XY)
        {
          if(!MassSpecDataFileLoaderBrukerXy::canLoadData(fileName))
            continue;
          else
            return iter;
        }
      else if(iter == MassSpecDataFileFormat::MASS_SPEC_DATA_FILE_FORMAT_MS1)
        {
          if(!MassSpecDataFileLoaderMs1::canLoadData(fileName))
            continue;
          else
            return iter;
        }
      else if(iter == MassSpecDataFileFormat::MASS_SPEC_DATA_FILE_FORMAT_XY)
        {
          if(!MassSpecDataFileLoaderXy::canLoadData(fileName))
            continue;
          else
            return iter;
        }
    }

  return MassSpecDataFileFormat::MASS_SPEC_DATA_FILE_FORMAT_NOT_SET;
}


} // namespace msXpSmineXpert
