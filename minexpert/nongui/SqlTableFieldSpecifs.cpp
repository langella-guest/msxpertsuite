/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#include <minexpert/nongui/SqlTableFieldSpecifs.hpp>

namespace msXpSmineXpert
{


//! Construct a SqlTableFieldSpecifs instance.
/*!

  The constructed instance is initialized with the parameters.

  \param tableName name of the table to configure.

  \param fieldName name of the field to configure.

  \param fieldQtType type of the data to be stored in the field (Qt-based
  naming scheme).

  \param fieldSqlType type of the data to be stored in the field (SQL-based
  naming scheme).

  \param comment comment string.

  \param sqlStatement SQL statement string.

*/
SqlTableFieldSpecifs::SqlTableFieldSpecifs(const QString &tableName,
                                           const QString &fieldName,
                                           FieldQtType fieldQtType,
                                           FieldSqlType fieldSqlType,
                                           const QString &comment,
                                           const QString &sqlStatement)
{
  m_tableName    = tableName;
  m_fieldName    = fieldName;
  m_fieldQtType  = fieldQtType;
  m_fieldSqlType = fieldSqlType;
  m_comment      = comment;
  m_sqlStatement = sqlStatement;
}


//! Construct a SqlTableFieldSpecifs instance.
/*!

  \param tableName name of the table to configure.

*/
SqlTableFieldSpecifs::SqlTableFieldSpecifs(const QString &tableName)
{
  m_tableName = tableName;
}

SqlTableFieldSpecifs::~SqlTableFieldSpecifs()
{
}


//! Set the name of the table.
void
SqlTableFieldSpecifs::setTableName(QString tableName)
{
  m_tableName = tableName;
}


//! Get the name of the table.
QString
SqlTableFieldSpecifs::tableName() const
{
  return m_tableName;
}


//! Set the name of the field in the table.
void
SqlTableFieldSpecifs::setFieldName(QString fieldName)
{
  m_fieldName = fieldName;
}


//! Get the name of the field in the table.
QString
SqlTableFieldSpecifs::fieldName() const
{
  return m_fieldName;
}


//! Set the type of the data to be recorded in the field (Qt-based naming).
void
SqlTableFieldSpecifs::setFieldQtType(FieldQtType type)
{
  m_fieldQtType = type;
}


//! Get the type of the data to be recorded in the field (Qt-based naming).
FieldQtType
SqlTableFieldSpecifs::fieldQtType() const
{
  return m_fieldQtType;
}


//! Set the type of the data to be recorded in the field (SQL-based naming).
void
SqlTableFieldSpecifs::setFieldSqlType(FieldSqlType type)
{
  m_fieldSqlType = type;
}


//! Get the type of the data to be recorded in the field (SQL-based naming).
FieldSqlType
SqlTableFieldSpecifs::fieldSqlType() const
{
  return m_fieldSqlType;
}


//! Set the comment.
void
SqlTableFieldSpecifs::setComment(QString comment)
{
  m_comment = comment;
}


//! Get the comment.
QString
SqlTableFieldSpecifs::comment() const
{
  return m_comment;
}


//! Set the SQL statement.
void
SqlTableFieldSpecifs::setSqlStatement(QString sqlStatement)
{
  m_sqlStatement = sqlStatement;
}


//! Get the SQL statemement.
QString
SqlTableFieldSpecifs::sqlStatement() const
{
  return m_sqlStatement;
}


} // namespace msXpSmineXpert
