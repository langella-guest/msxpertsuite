/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QObject>


/////////////////////// Local includes
#include "ui_OpenSpectraDlg.h"


namespace msXpSmineXpert
{

class AbstractPlotWidget;
class MassSpecDataSet;


//! The OpenSpectraDlg class provides a means to control open spectra.
/*!

  Each time a new mass data file is loaded, a new TIC chromatogram is computed
  and displayed in the TicChromWnd. When that operation is performed the
  TicChromPlotWidget is registered as a top plot widget in the data relationer
  (DataPlotWidgetRelationer) and a corresponding item is created in the list
  widget in this window.

  The list widget items thus represent all the open spectra. A drop down list
  allows to select menu items that will apply to the selected or unselected
  items.

*/
class OpenSpectraDlg : public QDialog
{
  Q_OBJECT

  private:
  //! Graphical interface definition.
  Ui::OpenSpectraDlg m_ui;

  //! Drop-down menu item string list.
  QStringList m_actionList{"Hide",
                           "Hide all but selected",
                           "Hide all",
                           "Show",
                           "Show all but selected",
                           "Show all",
                           "Toggle visibility",
                           "Change color",
                           "Close",
                           "Close all but selected",
                           "Close all"};

  //! Map relating the list widget items to the tic chromatogram plot widgets.
  QMap<QListWidgetItem *, AbstractPlotWidget *> m_listItemTicPlotMap;

  public:
  // Construction/destruction
  OpenSpectraDlg(QWidget *parent);
  ~OpenSpectraDlg();

  bool initialize();
  void closeEvent(QCloseEvent *event);
  void writeSettings();
  void readSettings();

  QListWidgetItem *addOpenSpectrum(const MassSpecDataSet *dataSet,
                                   QColor color);
  void actionComboBoxActivated(const QString &text);
  void removeListWidgetItem(AbstractPlotWidget *widget);
  bool isPlotWidgetSelected(AbstractPlotWidget *widget);
  int itemCount();
  int selectedItemCount();
  int firstSelectedPlotWidgetIndex(AbstractPlotWidget *widget = nullptr);
  AbstractPlotWidget *firstSelectedPlotWidget(int *index = nullptr);
  AbstractPlotWidget *plotWidgetAt(int index);
  AbstractPlotWidget *selectPlotWidget(int index);
  int selectPlotWidget(AbstractPlotWidget *widget);
  AbstractPlotWidget *togglePlotWidget(int index);
  int togglePlotWidget(AbstractPlotWidget *widget);
  int selectAllPlotWidgets();
  int deselectAllPlotWidgets();
  AbstractPlotWidget *deselectAllPlotWidgetsButOne(int index);
  int deselectAllPlotWidgetsButOne(AbstractPlotWidget *widget);

  QList<AbstractPlotWidget *> selectedPlotWidgets() const;
  QList<AbstractPlotWidget *> unselectedPlotWidgets() const;

  void keyReleaseEvent(QKeyEvent *event);
};


} // namespace msXpSmineXpert
