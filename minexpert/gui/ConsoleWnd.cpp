/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QLineEdit>


/////////////////////// Local includes
#include <minexpert/gui/ConsoleWnd.hpp>
#include <minexpert/gui/Application.hpp>


namespace msXpSmineXpert
{


//! Construct a ConsoleWnd instance.
ConsoleWnd::ConsoleWnd(QWidget *parent, const QString &applicationName)
  : QMainWindow(parent), m_applicationName{applicationName}
{
  if(parent == nullptr)
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

  m_ui.setupUi(this);

  // This Console window may be used by more than a single application, thus
  // set the app name along with the title.
  setWindowTitle(QString("%1 - Console").arg(m_applicationName));
  readSettings();
}


//! Destruct \c this ConsoleWnd instance.
ConsoleWnd::~ConsoleWnd()
{
  writeSettings();
}


void
ConsoleWnd::closeEvent(QCloseEvent *event)
{
  writeSettings();
  event->accept();
}


//! Save the settings to later restore the window in its same position.
void
ConsoleWnd::writeSettings()
{
  QSettings settings;
  settings.beginGroup("ConsoleWnd");
  settings.setValue("geometry", saveGeometry());
  settings.setValue("windowState", saveState());
  settings.setValue("visible", isVisible());
  settings.endGroup();
}


//! Read the settings to restore the window in its last position.
void
ConsoleWnd::readSettings()
{
  QSettings settings;
  settings.beginGroup("ConsoleWnd");
  restoreGeometry(settings.value("geometry").toByteArray());
  restoreState(settings.value("windowState").toByteArray());

  bool wasVisible = settings.value("visible").toBool();
  setVisible(wasVisible);

  settings.endGroup();
}


//! Log a message into the console.
/*!

  The logged message can be printed out according to the following parameters\:

  \param msg string containing the message to print out in the console window.

  \param color color to be used for printing the message.

  \param overwrite tells if the previous line should be overwritten with \msg.

*/
void
ConsoleWnd::logMessage(QString msg, const QColor &color, bool overwrite)
{
  // qDebug() << __FILE__ << "@" << __LINE__ << "ENTER" << __FUNCTION__ << "()"
  //<< "Message:" << msg;

  // If the color is valid, then use that color to show the text.
  if(color.isValid())
    {
      QTextCharFormat prevCharFormat =
        m_ui.consolePlainTextEdit->currentCharFormat();
      QTextCharFormat newCharFormat = prevCharFormat;

      newCharFormat.setForeground(QBrush(color));

      m_ui.consolePlainTextEdit->setCurrentCharFormat(newCharFormat);

      m_ui.consolePlainTextEdit->appendPlainText(msg);

      // Record the logged text!
      m_lastLoggedText = msg;

      m_ui.consolePlainTextEdit->setCurrentCharFormat(prevCharFormat);
    }
  else
    {
      // QTextCharFormat prevCharFormat =
      // m_ui.consolePlainTextEdit->currentCharFormat();
      // prevCharFormat.setForeground(QBrush(QColor("black")));
      // m_ui.consolePlainTextEdit->setCurrentCharFormat(prevCharFormat);

      m_ui.consolePlainTextEdit->appendPlainText(msg);

      // Record the logged text!
      m_lastLoggedText = msg;
    }

  // Make sure that the document scrolls.
  QTextCursor cursor = m_ui.consolePlainTextEdit->textCursor();
  cursor.movePosition(QTextCursor::End);
  m_ui.consolePlainTextEdit->setTextCursor(cursor);
};


//! Overload without color for scripting simplicity.
void
ConsoleWnd::logMessage(QString msg)
{
  logMessage(msg, QColor());
}

} // namespace msXpSmineXpert
