/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QObject>
#include <QString>
#include <QWidget>
#include <QScriptValue>

///////////////////////////// Local includes
#include <minexpert/gui/AbstractPlotWidget.hpp>
#include <minexpert/nongui/History.hpp>
#include <minexpert/nongui/MzIntegrationParams.hpp>


namespace msXpSmineXpert
{


//! The TicChromPlotWidget class provides a tic chromatogram-specific plot
//! widget.
class TicChromPlotWidget : public AbstractPlotWidget
{
  Q_OBJECT

  private:
  virtual QMenu *createContextMenu() override;

  public:
  QString craftAnalysisStanza(QCPGraph *theGraph      = Q_NULLPTR,
                              const QString &fileName = QString());

  int arbitraryIntegrationType() override;

  // Construction/Desctruction
  TicChromPlotWidget(QWidget *parent,
                     const QString &name,
                     const QString &desc,
                     const MassSpecDataSet *massSpecDataSet = Q_NULLPTR,
                     const QString &fileName                = QString(),
                     bool isMultiGraph                      = false);

  ~TicChromPlotWidget();

  void setFocus() override;

  void mouseReleaseHandledEvent(QMouseEvent *event) override;
  void keyReleaseEvent(QKeyEvent *event) override;
  void mousePressHandler(QMouseEvent *event) override;

  Q_INVOKABLE void jsSetMzIntegrationParams();
  Q_INVOKABLE QScriptValue jsMzIntegrationParams() const;

  void integrateToMz(double lower = qSNaN(), double upper = qSNaN());
  Q_INVOKABLE void jsIntegrateToMz(double lower = qSNaN(),
                                   double upper = qSNaN());

  void integrateToDt(double lower = qSNaN(), double upper = qSNaN());
  Q_INVOKABLE void jsIntegrateToDt(double lower = qSNaN(),
                                   double upper = qSNaN());

  void integrateToTicIntensity(double lower = qSNaN(), double upper = qSNaN());
  Q_INVOKABLE double jsIntegrateToTicIntensity(double lower = qSNaN(),
                                               double upper = qSNaN());

  void integrateToXic(double lower, double upper);
  Q_INVOKABLE void jsIntegrateToXic(double lower, double upper);

  Q_INVOKABLE QString statistics();

  Q_INVOKABLE void newPlot();


  signals:
  void recordAnalysisStanza(QString stanza, const QColor &color = QColor());

  void newMassSpectrum(const MassSpecDataSet *massSpecDataSet,
                       const QString &msg,
                       History history,
                       QColor color);

  void newDriftSpectrum(const MassSpecDataSet *massSpecDataSet,
                        const QString &msg,
                        History history,
                        QColor color);

  void ticIntensitySignal(const MassSpecDataSet *massSpecDataSet,
                          const QString &msg,
                          const History &history);

  // No newTicChromatogram signal here because that signal is coded in
  // AbstractMultiPlotWidget, since it is emitted from all derived classes.
};


} // namespace msXpSmineXpert
