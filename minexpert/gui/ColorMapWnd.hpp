/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QObject>
#include <QString>
#include <QWidget>
#include <QColor>
#include <QHash>


/////////////////////// Local includes
#include <minexpert/gui/AbstractMultiPlotWnd.hpp>
#include <minexpert/gui/ColorMapPlotWidget.hpp>
#include <minexpert/nongui/AnalysisPreferences.hpp>
#include <minexpert/nongui/MassSpecDataSet.hpp>
#include <minexpert/nongui/MassDataIntegrator.hpp>


namespace msXpSmineXpert
{


//! The ColorMapWnd class provides a window to display ion mobility data.
/*!

  When opening a mass data file that contains ion mobility data, the program
  automatically computes a color map and displays that colormap in a window of
  this class. There is only one such window in a running mineXpert program.
  All the color map plot wigets (see ColorMapPlotWidget) are inserted in \c
  this ColorMapWnd' layout.

  The color map that is computed right after loading the ion mobility mass
  spectrometry data file displays the mass spectra (ordinates\: y-axis) as a
  function of the drift time at which they were acquired (abscissae\: x-axis).
  The intensity corresponding the TIC of each m/z value in the spectra is
  represented as a color.

  \sa ColorMapPlotWidget

*/
class ColorMapWnd : public AbstractMultiPlotWnd
{
  Q_OBJECT

  private:
  // The ColorMapWnd has no multigraph plot widget because it does not make any
  // sense to overlay two color maps.

  AbstractPlotWidget *
  addPlotWidget(const QVector<double> &keyVector,
                const QVector<double> &valVector,
                const QString &desc,
                const QColor &color                    = Qt::black,
                const MassSpecDataSet *massSpecDataSet = Q_NULLPTR,
                bool isMultiGraph                      = false);

  void setupWindow();

  QAction *mp_transposeAxesAct = Q_NULLPTR;

  //! Number of plot widgets.
  /*!

    This counter logs the number of plot widgets and is used by the
    scripting framework to provide a numerical suffix to the plot widget
    name in the scripting environment.

*/
  static int widgetCounter;

  public:
  ColorMapWnd(QWidget *parent);
  ~ColorMapWnd();

  Q_INVOKABLE void
  hide()
  {
    QMainWindow::hide();
  };
  Q_INVOKABLE void
  show()
  {
    QMainWindow::show();
  };

  void initialColorMap(const MassSpecDataSet *massSpecDataSet,
                       AbstractPlotWidget *originPlotWidget,
                       const QColor &color = Qt::black);

  void jsInitialColorMap(const MassSpecDataSet *massSpecDataSet,
                         AbstractPlotWidget *originPlotWidget,
                         const QColor &color = Qt::black);

  void ticIntensity(const MassSpecDataSet *massSpecDataSet,
                    const QString &msg,
                    History history);

  public slots:

  void initialColorMapDone(MassDataIntegrator *integrator,
                           AbstractPlotWidget *originWidget,
                           QColor color);
  void ticIntensityDone(MassDataIntegrator *integrator,
                        QString msg,
                        AbstractPlotWidget *senderPlotWidget);

  public slots:

  signals:

  void integrateToTicIntensitySignal();

  void initialColorMapDoneSignal(AbstractPlotWidget *widget,
                                 AbstractPlotWidget *originWidget,
                                 const MassSpecDataSet *massSpecDataSet,
                                 QColor color);
};

} // namespace msXpSmineXpert
