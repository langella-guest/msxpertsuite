function linearRegression(xArray, yArray)
{
	var lr = {};
	var n = yArray.length;
	var sum_x = 0;
	var sum_y = 0;
	var sum_xy = 0;
	var sum_xx = 0;
	var sum_yy = 0;

	for(var i = 0; i < yArray.length; i++)
	{
		sum_x += xArray[i];
		sum_y += yArray[i];
		sum_xy += (xArray[i] * yArray[i]);
		sum_xx += (xArray[i] * xArray[i]);
		sum_yy += (yArray[i] * yArray[i]);
	} 

	lr['slope'] = (n * sum_xy - sum_x * sum_y) / (n*sum_xx - sum_x * sum_x);
	lr['intercept'] = (sum_y - lr.slope * sum_x)/n;
	lr['r2'] = Math.pow((n*sum_xy - sum_x*sum_y)/Math.sqrt((n*sum_xx-sum_x*sum_x)*(n*sum_yy-sum_y*sum_y)),2);

	return lr;
}
