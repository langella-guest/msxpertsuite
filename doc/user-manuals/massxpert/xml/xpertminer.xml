<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE chapter [

<!ENTITY % entities SYSTEM "msxpertsuite-user-manuals.ent">
%entities;

<!ENTITY % sgml.features "IGNORE">
<!ENTITY % xml.features "INCLUDE">
<!ENTITY % dbcent PUBLIC "-//OASIS//ENTITIES DocBook Character Entities V4.5//EN"
"/usr/share/xml/docbook/schema/dtd/4.5/dbcentx.mod">
%dbcent;
]>

<chapter
	xml:id="chap_xpertminer-module" 
	xmlns="http://docbook.org/ns/docbook" version="5.0"
	xmlns:xlink="http://www.w3.org/1999/xlink">

	<info>

		<title>XpertMiner: a Data Miner</title>

		<keywordset>
			<keyword>&mz; list</keyword>
			<keyword>action-formula</keyword>
		</keywordset>

	</info>

	<para> &xpm; is a module that has been conceived as a repository of
		functionalities aimed at analyzing mass data. The data to be subjected to
		mining can originate from:</para>

	<itemizedlist>
		<listitem>
			<para>

				&massXp;-based simulations, like polymer cleavage, oligomer
				fragmentation or arbitrary mass searches;

			</para>
		</listitem>
		<listitem>
			<para>

				An export of a mass list from the mass spectrometer software;

			</para>
		</listitem>
		<listitem>
			<para>

				Any mass data that might have been processed outside of &massXp; and
				that need to be reimported in &xpm;.

			</para>
		</listitem>
	</itemizedlist>

	<sect1 xml:id="sec_xpertminer-invocation">

		<title>XpertMiner Invocation</title>

		<para> The &xpm; module is easily called by pulling down the
			<guimenu>XpertMiner</guimenu> menu item from the &massXp; program's menu.
			Clicking on
			<guimenu>XpertMiner</guimenu>&rarr;<guimenuitem>mzLab</guimenuitem> will
			open the mzLab window, as represented in <xref
				linkend="fig_xpertminer-mzlab-wnd"/>.</para>

	</sect1>

	<sect1 xml:id="sec_xpertminer-mining-mz-ratios">

		<title>mzLab: Mining &mz; Ratios</title>

		<para> The features available in this laboratory operate on lists of &mz;
			values in the form of (&mz;,z) pairs. The mass of the ion is represented by
			<quote>m</quote>, while <quote>z</quote> is the charge of the ion.  With
			the two data in the pair, the &mz; ratio and the z charge, and knowing the
			ionization rule that ionized the analyte in the first place, it is
			possible to perform any mass calculation on the (&mz;,z) pair.</para>

		<para> The mzLab window is represented in <xref
				linkend="fig_xpertminer-mzlab-wnd"/>. This window is divided into a
			number of  distinct parts:</para>

		<itemizedlist>

			<listitem>

				<para> The left part (<guilabel>Working lists</guilabel>) contains two
					list widgets which will hold the names of the different working &mz;
					lists. We call these lists <quote>catalogues</quote> in the following
					text; </para>

			</listitem>
			<listitem>

				<para> The <guilabel>Default ionization</guilabel> groupbox widget contains
					the ionization rule that is to be assumed when working on (&mz;,z)
					pairs. If you are unsure about this concept, please read section
					<xref linkend="sec_polymer-chemical-entities"/>.  </para>

			</listitem>

			<listitem>

				<para> The <guilabel>Actions on a single list</guilabel> groupbox
					widget holds a number of mining actions to be performed on a single
					list that is identified by being selected either in the <guilabel>Cat.
						1</guilabel> or in the <guilabel>Cat. 2</guilabel> catalogue of
					available &mz; lists.</para>

				<para>When performing computations that modify the &mz; values in the
					list, if <guilabel>Perform computation in place</guilabel> is checked,
					then the new &mz; values will replace the former ones.  Otherwise, the
					program will ask for a new &mz; list name as a new list is created to
					hold the new &mz; values resulting from the computation.  </para>

				<para>There are two main kinds of computations that might be performed
					against a single &mz; list:</para>

				<itemizedlist>

					<listitem>

						<para><guilabel>Mass-based actions</guilabel> rely on masses or &mz;
							values to perform computations;</para>

					</listitem>

					<listitem>

						<para><guilabel>Formula-based actions</guilabel> rely on
							formul&aelig; to perform computations;</para>

					</listitem>

				</itemizedlist>

			</listitem>

			<listitem>

				<para>The <guilabel>Actions on multiple lists</guilabel> groupbox widget
					allows one to perform actions that use two lists, for example matching
					masses (or &mz; ratios) in two lists with a given tolerance.</para>

			</listitem>

		</itemizedlist>

		<figure xml:id="fig_xpertminer-mzlab-wnd">

			<title>mzLab window</title>

			<mediaobject>

				<imageobject role="fo">
					<imagedata fileref="print-xpertminer-mzlab-wnd.png" format="PNG" scale="100"/>
				</imageobject>
				<imageobject role="html">
					<imagedata fileref="web-xpertminer-mzlab-wnd.png" format="PNG" scale="80"/>
				</imageobject>

				<caption>

					<para>XpertMiner's laboratory window. From there it is possible to
						create any number of &mz; list dialog windows, to fill-in &mz; data
						and start making computations, like changing the ionization;
						applying arbitrary masses, formul&aelig; or &mz; ratios; matching
						masses or &mz; ratios&hellip;. See text for details.  </para>

				</caption>

			</mediaobject>

		</figure>

	</sect1>

	<sect1 xml:id="sec_xpertminer-create-new-input-mz-list">

		<title>Creating a New Input &mz; List</title>

		<para> In order to be able to use the mzLab, it is necessary to create at
			least one list of (&mz;,z) pairs, which is referred to by <quote>input
				&mz; list</quote>, for short. To create a new input &mz; list, you click
			<guilabel>New list</guilabel>. An input dialog window let's you enter the
			name of the new list. The new input &mz; list dialog window shows up empty
			like in <xref linkend="fig_xpertminer-empty-input-mz-list"/>.  That kind
			of list is actually a table view widget that is embedded in a dialog
			window.  The first column of the table view widget holds the &mz; value,
			and the second column, the z value. Optionally, the name of the
			corresponding oligomer and its coordinates in the polymer sequence can be
			shown, depending on how data have been imported into the &mz; list (see
			below).</para>

		<figure xml:id="fig_xpertminer-empty-input-mz-list">

			<title>&mz; list's empty input &mz; list dialog window.</title>

			<mediaobject>

				<imageobject role="fo">
					<imagedata fileref="print-xpertminer-empty-input-mz-list.png" format="PNG" scale="100"/>
				</imageobject>
				<imageobject role="html">
					<imagedata fileref="web-xpertminer-empty-input-mz-list.png" format="PNG" scale="80"/>
				</imageobject>

				<caption>

					<para>An empty input &mz; list dialog window is empty upon its
						creation.  Filling that list is performed by either drag-and-drop or
						clipboard operations.  </para>

				</caption>

			</mediaobject>

		</figure>


		<tip>

			<para>The list name entered by the user  at creation time will be used to
				refer to that list in the two catalogues.</para>

		</tip>

		<sect2 xml:id="sec_xpertminer-fill-input-mz-lists">

			<title>Filling &mz; Lists with Data</title>

			<para> Once a new input &mz; list has been named and created, it is
				necessary to fill it with (&mz;,z)  pairs. This is performed
				<emphasis>via</emphasis> drag-and-drop or clipboard operations. There
				might be a number of different data sources to be used for filling the
				input &mz; list, all reviewed in the following sections.</para>

			<sect3>

				<title>Importing Data from &massXp; Results Windows</title>

				<para> Data from the various simulations available in &massXp; include
					cleavage results, fragmentation results and mass search results, which
					all produce oligomers that are displayed in treeview widgets, as
					shown in <xref linkend="fig_xpertedit-cleavages"/> or <xref
						linkend="fig_xpertedit-fragmentation"/> or <xref
						linkend="fig_xpertedit-mass-search"/>. </para>

				<para>From these results windows, either select the oligomers of
					interest and export these data to the clipboard or perform a
					drag-and-drop operation to the &mz;&nbsp;list area.  Both ways produce
					identical results, as described in <xref
						linkend="fig_xpertminer-filled-input-mz-list"/>.  One can see that
					the mass of the oligomers is set in the list, along with the charge,
					the oligomer name and, finally, the coordinates of the oligomer in the
					corresponding polymer.</para>

				<figure xml:id="fig_xpertminer-filled-input-mz-list">

					<title>&mz;&nbsp;list's data-filled input &mz; list dialog window</title>

					<mediaobject>

						<imageobject role="fo">
							<imagedata fileref="print-xpertminer-filled-input-mz-list.png" format="PNG" scale="100"/>
						</imageobject>
						<imageobject role="html">
							<imagedata fileref="web-xpertminer-filled-input-mz-list.png" format="PNG" scale="80"/>
						</imageobject>

						<caption>

							<para>Data pasted from any &massXp; result window hold all the
								necessary data to fill-in fully the &mz;&nbsp;list. The user is
								asked to specified if the imported data are for monoisotopic or
								average masses.</para>

						</caption>

					</mediaobject>

				</figure>

			</sect3>

			<sect3>

				<title>Textual Data from Non-&massXp; Results Windows, Without Charge</title>

				<para> The mass data might be first copied to the clipboard from other
					software and then imported in the &mz;&nbsp;list by clicking
					<guilabel>Data from clipboard</guilabel> (a drag-and-drop operation
					would also work).</para>

				<para>When the charge z is not present in the imported &mz; list, then
					it is deduced from the ionization rule currently defined the the mzLab
					window (see background of&nbsp;<xref
						linkend="fig_xpertminer-mz-only-filled-input-mz-list"/>).</para>

				<figure xml:id="fig_xpertminer-mz-only-filled-input-mz-list">

					<title>&mz;&nbsp;list's (&mz;) textual data-filled input &mz; list
						dialog window.</title>

					<mediaobject>

						<imageobject role="fo">
							<imagedata fileref="print-xpertminer-mz-only-filled-input-mz-list.png" format="PNG" scale="100"/>
						</imageobject>
						<imageobject role="html">
							<imagedata fileref="web-xpertminer-mz-only-filled-input-mz-list.png" format="PNG" scale="80"/>
						</imageobject>

						<caption>

							<para>When mass data missing the z charge value are pasted from
								the clipboard, the z value is assumed to be the result of the
								ionization rule defined in the mzLab window (the window on the
								background). See text for details.  </para>

						</caption>

					</mediaobject>

				</figure>

			</sect3>

			<sect3>

				<title>Textual Data From Non-&massXp; Results Windows, With z Charge Specified</title>

				<para>When the mass data copied to the clipboard do include the &mz;
					ratio along with the z charge, the delimiter character needs to be
					known. This character must be set as the <guilabel>Field
						delimiter</guilabel> in the &mz;&nbsp;list prior to clicking
					<guilabel>Data from clipboard</guilabel> to actually import the data.
					This way, the program knows how to parse the (&mz;,z) pairs. A
					drag-and-drop operation from a graphical text editor would have
					produced the same results.  The obtained list is shown in &nbsp;<xref
						linkend="fig_xpertminer-mz-z-filled-input-mz-list"/>.</para>


				<figure xml:id="fig_xpertminer-mz-z-filled-input-mz-list">

					<title>&mz;&nbsp;list's (&mz;,z) textual data-filled input &mz; list
						dialog window.</title>

					<mediaobject>

						<imageobject role="fo">
							<imagedata fileref="print-xpertminer-mz-z-filled-input-mz-list.png" format="PNG" scale="100"/>
						</imageobject>
						<imageobject role="html">
							<imagedata fileref="web-xpertminer-mz-z-filled-input-mz-list.png" format="PNG" scale="80"/>
						</imageobject>

						<caption>

							<para>The textual data contain (&mz;,z) pairs (delimited with
								<quote>$</quote>).  The list contains both &mz; and z data. See text for details.  </para>

						</caption>

					</mediaobject>

				</figure>

			</sect3>


			<sect3>

				<title>General Rules on Textual Mass Data Format</title>

				<para>The most detailed format that is supported is the following:</para>

				<para xml:id="para_xpertminer-imported-data-format">

					m/z &lt;delim&gt; charge &lt;delim&gt; name &lt;delim&gt; coordinates &lt;delim&gt;

				</para>

				<para> In this syntax, the &lt;delim&gt; (field delimiter) is the
					<quote>$</quote> character. Any character might be used (including
					spaces). The delimiter character (or string) that is set in the
					&mz;&nbsp;list window must be the same as the one defined in the
					window from where the data originate (when using the option to export
					the selected oligomers data to the clipboard).</para>


				<para> For example, data can be formatted like this:</para>

				<screen>

					3818.05262$1$0#2#z=1$[3-39]
					3834.05262$1$0#2#z=1$[3-39]

				</screen>


				<para> The compulsory datum (that is, the imported datum, either
					dragged and dropped or pasted from the clipboard), is the
					<emphasis>m/z ratio</emphasis>. The charge, name, coordinates
					fields are optional. If the charge is present, it will be taken
					into account while preparing the data for further use by the
					&mz;&nbsp;list. If the charge is absent, it is deduced from the
					ionization rule currently defined in the mzLab window
					(&nbsp;<xref linkend="fig_xpertminer-mzlab-wnd"/>).</para>

				<warning>

					<para>If there is no charge value, then the other name and coordinates
						fields cannot be filled (or an error will result).  The presence of
						the name and coordinates fields is optional.  Note, however that the
						coordinates field is <emphasis>fundamental</emphasis> to be able to
						highlight the corresponding region in the &xpe; sequence editor upon
						double-clicking of any given item in the &mz; list. For this to be
						possible, the data must have been originated by drag and drop from a
						&massXp; simulation results window <emphasis>or</emphasis> the
						&mz;&nbsp;list window must have been connected to a polymer sequence
						editor window (see below).</para>

				</warning>

			</sect3>

		</sect2>

		<sect2 xml:id="sec_xpertminer-impose-mass-type-mono-avg">

			<title>Imposing the Mass Type: Mono Or Avg</title>

			<para> When dropping data&mdash;either from &massXp;-driven simulations
				(cleavage, fragmentation or mass search) or from textual data
				originating from outside &massXp;&mdash;it is necessary to inform the
				input &mz; list of what kind of mass it is dealt with. That is, when
				dropping a line like <quote>1234.56 1</quote>, the question is:
				&mdash;<quote>The &mz; 1234.56 value is a monoisotopic &mz; or an
					average &mz;?</quote> The type of the masses dropped in an input &mz;
				list is governed by the two radio buttons labelled
				<guilabel>Mono</guilabel> and <guilabel>Avg</guilabel>. The one of the
				two radiobuttons that is checked at the moment the drop or the
				clipboard-paste occurs determines the type of the masses that are dealt
				with. It will be possible to check the other radio button widget once a
				first data drop occurred, but then the user will be alerted about doing
				so, as this has huge implications for the calculations to be performed
				later.</para>

		</sect2>

	</sect1>


	<sect1 xml:id="sec_xpertminer-work-on-input-mz-list">

		<title>Working on One Input &mz; List</title>

		<para> Once an input &mz; list has been filled with data, it becomes
			possible to perform calculations on these data. Because there might be any
			number of input &mz; lists open at any given time, it is necessary to
			identify the input &mz; list onto which to perform these calculations. The
			selection of the input &mz; list(s) is performed in two steps: first, by
			indicating in which catalogue the list of interest is currently selected
			(select either <guilabel>Cat.1</guilabel> or <guilabel>Cat.3</guilabel>).
			Make sure a list name is currently selected in the proper catalogue.</para>

		<sect2>

			<title>Available Calculations</title>

			<para> There are a number of operations that might be performed, all of
				which are selectable in the <guilabel>Actions on a single
					list</guilabel> groupbox widget. The simulations are organized into
				two groups: </para>

			<itemizedlist>

				<listitem>
					<para>

						<guilabel>Formula-based</guilabel> actions which involve processing
						the input &mz; lists with formul&aelig; (that is, chemical entities
						represented using formul&aelig;):

					</para>

					<itemizedlist>

						<listitem>
							<para>

								<guilabel>Apply formula</guilabel> will modify the
								&mz;&nbsp;list by applying to all of its members the mass
								corresponding to the formula entered by the user.  This is where
								it is crucial that the mass type (mono or avg) be set correctly,
								because the type of the mass calculated for the formula must be
								of the same type as the type of the data;

							</para>
						</listitem>
						<listitem>
							<para>

								<guilabel>Increment charge by</guilabel> will iterate in all the
								items present in the list and apply the charge increment to
								them. For example, one item in the list that is charged 1 will
								be deionized and reionized to 2 (this calculation involves the
								ionization rule of the oligomer, and thus its ionization
								formula);

							</para>
						</listitem>
						<listitem>
							<para>

								<guilabel>Reionization</guilabel> will iterate in all the items
								present in the list and apply the new ionization rule, defined
								in this groupbox widget.

							</para>
						</listitem>

					</itemizedlist>

				</listitem>

				<listitem>
					<para>

						<guilabel>Mass-based</guilabel> actions which involve processing the
						input &mz; lists with numerical data representing masses:

					</para>

					<itemizedlist>

						<listitem>
							<para>

								<guilabel>Apply mass</guilabel> will iterate in all the items
								present in the list and apply the entered mass to them. The
								value entered by the user is a <emphasis>mass</emphasis>, not a
								&mz; ratio. Thus, this computation involves, for each (&mz;,z)
								pair in the list the sequential deionization, mass addition,
								reionization.

							</para>
						</listitem>
						<listitem>
							<para>

								<guilabel>Apply threshold</guilabel> will remove all data items
								in the list for which &mz; or M is less than the value set,
								depending on the radio button that is selected (<guilabel>On m/z
									value</guilabel> or <guilabel>On M value</guilabel>).

							</para>
						</listitem>

					</itemizedlist>

				</listitem>

			</itemizedlist>

		</sect2>

		<sect2>

			<title>Output of the Calculations</title>

			<para> Simulations performed on a single input &mz; list produce
				a &mz; list that is identical to the input list, unless for the
				m and/or z values, which might have changed. This means that
				it is perfectly possible to:</para>

			<itemizedlist>
				<listitem>
					<para>

						Overwrite the initial data with the newly obtained ones (this is performed by
						checking the <guilabel>Perform computation in place</guilabel> check button widget);

					</para>
				</listitem>
				<listitem>
					<para>

						Create a new list with the newly obtained data. As a convenience for
						the user, the new list will be an input &mz; list in which it will
						be possible to perform ulterior simulations. This is useful when the
						simulations that need to be performed are sequential in kind. To
						have a new list created uncheck <guilabel>Perform computation in
							place</guilabel>.

					</para>
				</listitem>
			</itemizedlist>

		</sect2>

		<sect2>

			<title>Internal Workings</title>

			<para> When an operation is performed on the items of an input &mz; list,
				say we want to make sodium adducts (that would be a formula
				<quote>-H+Na</quote>) of all the items in the list, the process involves
				the following steps, as detailed below for one single item of the list
				(which has data pair (334.341,3) and <guilabel>protonation</guilabel> as
				ionization agent).</para>

			<itemizedlist>
				<listitem>
					<para>

						Convert the tri-protonated analyte into a non-ionized analyte, thus
						getting M=1000;

					</para>
				</listitem>
				<listitem>
					<para>

						Compute the mass of the <quote>-H+Na</quote> formula: 21.98 Da;

					</para>
				</listitem>
				<listitem>
					<para>

						Add 1000+21.98;

					</para>
				</listitem>
				<listitem>
					<para>

						Reionize to the initial charge state: (341.67,3).

					</para>
				</listitem>
			</itemizedlist>

		</sect2>

	</sect1>

	<sect1 xml:id="sec_xpertminer-work-two-input-mz-lists">

		<title>Working on Two Input &mz; Lists</title>

		<para> It is possible to perform calculations on two input &mz; lists. These
			calculations are called matches. The (&mz;,z) pairs of two different input
			&mz; lists might be matched. Typically, a match operation would involve
			data from the mass spectrometer and data from a &massXp;-based simulation
			(cleavage or fragmentation, for example). In order to perform a match
			operation, the first input &mz; list (the data from the mass spectrometer)
			should be selected by its name in the <guilabel>Catalogue&nbsp;</guilabel>
			list and the second input &mz; list (the data from the simulation) should
			be selected by its name in the <guilabel>Catalogue&nbsp;2</guilabel> list.
			Note that if the two input &mz; lists are not of the same type (one is
			mono and the other is avg), the user will be alerted about this
			point.</para>

		<sect2 xml:id="sec_xpertminer-output-calculations">


			<title>Output of The Calculations</title>

			<para> Calculations involving matches between two input lists produce an
				output that is displayed in an output &mz; list, which is different from
				an input &mz; list. <xref
					linkend="fig_xpertminer-mz-z-filled-output-mz-list"/> shows the
				results after having performed a match operation between an input &mz;
				list obtained from the mass spectrometer
				(<guilabel>Catalogue&nbsp;1</guilabel>) and an input &mz; list obtained
				by simulating a cleavage with trypsin
				(<guilabel>Catalogue&nbsp;2</guilabel>). The output &mz; list dialog
				window holds all the matches along with the original data and the
				error.</para>

			<figure xml:id="fig_xpertminer-mz-z-filled-output-mz-list">

				<title>Match operation between two &mz;&nbsp;lists, output list dialog window.</title>

				<mediaobject>

					<imageobject role="fo">
						<imagedata fileref="print-xpertminer-mz-z-filled-output-mz-list.png" format="PNG" scale="100"/>
					</imageobject>
					<imageobject role="html">
						<imagedata fileref="web-xpertminer-mz-z-filled-output-mz-list.png" format="PNG" scale="80"/>
					</imageobject>

					<caption>

						<para>See text for details
						</para>

					</caption>

				</mediaobject>

			</figure>

		</sect2>

		<sect2 xml:id="sec_xpertminer-tracing-data">

			<title>Tracing the Data</title>

			<para> When the data used for filling an input &mz; list come from a
				&massXp;-based simulation it is possible to trace back the (&mz;,z)
				pair items to the corresponding sequence in the polymer sequence
				editor that gave rise to these oligomers in the first place. This
				is only possible if:</para>

			<itemizedlist>
				<listitem>
					<para>

						The way the data were fed into the input &mz; list was by dragging oligomers from
						the treeview widgets, as described earlier;

					</para>
				</listitem>
				<listitem>
					<para>

						The polymer sequence window is still opened when the tracing back is tried.

					</para>
				</listitem>
			</itemizedlist>

			<para> If the data do not originate immediately from the
				&massXp;-based simulations (that is, the data do not originate
				directly from results treeview of cleavages or fragmentations
				or mass searches), it is still possible to perform the
				highlighting of corresponding oligomers in the sequence editor
				window, provided that the following requirements are
				met:</para>

			<itemizedlist>
				<listitem>
					<para>

						The data imported in the &mz; list of &mz;&nbsp;list are rich, that
						is, they comprise the coordinates data in the right format (see the
						data format, <xref linkend="para_xpertminer-imported-data-format"/>;

					</para>
				</listitem>
				<listitem>
					<para>

						The proper polymer sequence is opened in a sequence editor window;

					</para>
				</listitem>
				<listitem>
					<para>

						The identifier in the <guilabel>This window identifier</guilabel>
						line edit widget of the sequence editor window above is copied into
						the <guilabel>Sequence editor identifier</guilabel> line edit
						widget.

					</para>
				</listitem>
			</itemizedlist>

			<para> In the case the above conditions are met, double-clicking
				onto a item of the &mz; list will highlight the corresponding
				sequence region in the sequence editor window.</para>

			<para> In order to trace back any given item in an input or in
				an output &mz; list to its corresponding polyemr sequence, just
				activate the item while having a look at the polymer sequence
				whence the oligomers initially originated. Each time an item
				is activated by double-clicking it, its corresponding sequence
				region will be highlighted (selected, actually) in the polymer
				sequence.</para>

		</sect2>

	</sect1>

</chapter>
