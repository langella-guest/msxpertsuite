[Setup]
AppName=msXpertSuite

; Set version number below
#define public version "5.8.7"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win7+"
#define sourceDir "C:\msys64\home\polipo\devel\msxpertsuite\development"

; Set version number below
AppVerName=msXpertSuite version {#version}
DefaultDirName={pf}\msXpertSuite
DefaultGroupName=msXpertSuite
OutputDir="C:\msys64\home\polipo\devel\msxpertsuite\development\winInstaller"

; Set version number below
OutputBaseFilename=msXpertSuite-{#arch}-{#platform}-v{#version}-setup

; Set version number below
OutputManifestFile=msXpertSuite-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}\doc\COPYING"
AppCopyright="Copyright (C) 2016- Filippo Rusconi"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments="msXpertSuite, by Filippo Rusconi"
AppContact="Filippo Rusconi, PhD, Scientist at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
WizardImageFile="{#sourceDir}\images\splashscreen-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}\data\massxpert"
Name: "{app}\doc"

[Files]
Source: "C:\msXpertSuite-libDeps\*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}\doc\history.html"; DestDir: {app}\doc; Components: massxpComp minexpComp docComp
Source: "{#sourceDir}\doc\history.odt"; DestDir: {app}\doc; Components: massxpComp minexpComp docComp
Source: "{#sourceDir}\doc\readme.txt"; DestDir: {app}\doc; Components: massxpComp minexpComp docComp

; ;;;;;;;;;;;;;;;;;;;;;;  massXpert ;;;;;;;;;;;;;;;;;;;;;;

Source: "{#sourceDir}\..\build-area\massxpert\massxpert.exe"; DestDir: {app}; Components: massxpComp 
Source: "{#sourceDir}\massxpert\data\*"; DestDir: {app}\data\massxpert; Flags: recursesubdirs; Components: massxpComp
; The PDF version of the user manual
Source: "{#sourceDir}\doc\user-manuals\massxpert-doc.pdf"; DestDir: {app}\doc\massxpert; Components: massxpComp docComp
; The HTML version of the user manual
Source: "{#sourceDir}\doc\user-manuals\massxpert\build\massxpert-user-manual\html\massxpert-user-manual\*"; DestDir: {app}\doc\massxpert\html; Flags: ignoreversion recursesubdirs; Components: minexpComp docComp

; ;;;;;;;;;;;;;;;;;;;;;;  mineXpert ;;;;;;;;;;;;;;;;;;;;;;

Source: "{#sourceDir}\..\build-area\minexpert\minexpert.exe"; DestDir: {app}; Components: minexpComp
; The PDF version of the user manual
Source: "{#sourceDir}\doc\user-manuals\minexpert-doc.pdf"; DestDir: {app}\doc\minexpert; Components: minexpComp docComp
; The HTML version of the user manual
Source: "{#sourceDir}\doc\user-manuals\minexpert\build\minexpert-user-manual\html\minexpert-user-manual\*"; DestDir: {app}\doc\minexpert\html; Flags: ignoreversion recursesubdirs; Components: minexpComp docComp

[Icons]
Name: "{group}\massXpert"; Filename: "{app}\massxpert.exe"; WorkingDir: "{app}"
Name: "{group}\mineXpert"; Filename: "{app}\minexpert.exe"; WorkingDir: "{app}"
Name: "{group}\Uninstall msXpertSuite"; Filename: "{uninstallexe}"

[Types]
Name: "msxpsType"; Description: "Full installation"
Name: "xxpType"; Description: "Custom installation with only selected software components"; Flags: iscustom

[Components]
Name: "massxpComp"; Description: "massXpert files only and related documentation"; Types: msxpsType xxpType
Name: "minexpComp"; Description: "mineXpert files only and related documentation"; Types: msxpsType xxpType
Name: "docComp"; Description: "All documentation files."; Types: msxpsType xxpType

[Run]
Filename: "{app}\massxpert.exe"; Description: "Launch massXpert"; Flags: postinstall nowait unchecked
Filename: "{app}\minexpert.exe"; Description: "Launch mineXpert"; Flags: postinstall nowait unchecked
