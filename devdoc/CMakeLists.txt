message("")
message(STATUS "${BoldGreen}Starting configuring of the devdoc material${ColourReset}")
message("")

# The developer documentation.
if(UNIX AND NOT APPLE)

	# Command:
	# make devdoc
	add_custom_target(devdoc
		COMMAND doxygen msxpertsuite.doxyconf
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
		COMMENT "Doxygen-based developer documentation generation")

	# Make sure we use the qtchooser command to run the qhelpgenerator command
	# qtchooser --run-tool=qhelpgenerator -qt=5 -o html/msxpertsuite.qch html/index.qhp

	# Command:
	# make qthelp
	add_custom_target(qthelp
		COMMAND qtchooser --run-tool=qhelpgenerator -qt=5 -o html/msxpertsuite.qch html/index.qhp
		COMMAND touch /tmp/this-is-the-touch
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
		COMMENT "Build the Qt Assistant indexed file.")

	# Make sure the qtAssistantHelp target depends on the devdoc target since
	# html/index.qhp is created upon making devdoc.
	add_dependencies(qthelp devdoc)

endif(UNIX AND NOT APPLE)

message("")
message(STATUS "${BoldGreen}Finished configuring of the devdoc material${ColourReset}")
message("")

